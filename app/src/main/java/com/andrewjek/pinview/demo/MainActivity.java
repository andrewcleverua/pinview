package com.andrewjek.pinview.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import com.andrewjek.pinvew.PinView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final PinView pinView = (PinView) findViewById(R.id.pin);
        final CheckBox checkBox = (CheckBox) findViewById(R.id.check_box);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pinView.setHidePin(!checkBox.isChecked());
            }
        });
    }
}
