package com.andrewjek.pinvew;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.view.GestureDetectorCompat;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.InputMethodManager;

import com.andrewjek.pinvewlib.R;

/**
 * Created by andrew on 13.10.15.
 */
public class PinView extends View implements View.OnKeyListener, GestureDetector.OnGestureListener {
    private static final int DEFAULT_COLOR = Color.BLACK;
    private static final int DEFAULT_PIN_COUNT = 4;
    private static final boolean DEFAULT_HIDE_PIN = true;
    private static final float DEFAULT_SCALE_SIZE = 1.0f / 3;
    private static final float EDIT_MODE_OFFSET = 14;
    private int mPinCount;
    private int mPinColor;
    private String mPin;
    private boolean hidePin;
    private float scalePinSize, scaleDotSize;
    private TextPaint mPinPaint;
    private int mWidth, mHeight;
    private float mRawPinHeight;
    private float mDividerPinSize;
    private GestureDetectorCompat gestureDetector;
    private OnKeyListener onKeyListener;
    private float mPinSize, mDotSize;

    public PinView(Context context) {
        this(context, null);
    }

    public PinView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PinView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PinView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PinView, defStyleAttr, defStyleRes);
        mPinCount = ta.getInt(R.styleable.PinView_pinCount, DEFAULT_PIN_COUNT);
        mPin = ta.getString(R.styleable.PinView_pin);
        validatePin();
        mPinColor = ta.getColor(R.styleable.PinView_pinColor, DEFAULT_COLOR);
        hidePin = ta.getBoolean(R.styleable.PinView_hidePin, DEFAULT_HIDE_PIN);
        scalePinSize = ta.getFloat(R.styleable.PinView_scalePinSize, -1);
        scaleDotSize = ta.getFloat(R.styleable.PinView_scaleDotSize, -1);
        ta.recycle();
        calculateScaleSize();
        mPinPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        mPinPaint.setDither(true);
        mPinPaint.setStyle(Paint.Style.FILL);
        mPinPaint.setColor(mPinColor);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setClickable(true);
        super.setOnKeyListener(this);
        gestureDetector = new GestureDetectorCompat(context, this);
    }

    private void calculateScaleSize() {
        if (scalePinSize == -1 && scaleDotSize == -1) {
            scaleDotSize = scalePinSize = DEFAULT_SCALE_SIZE;
        } else if (scalePinSize == -1 || scaleDotSize == -1) {
            float scale = Math.max(scaleDotSize, scalePinSize);
            scaleDotSize = scalePinSize = scale;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
        super.onInitializeAccessibilityEvent(event);
        event.setClassName(PinView.class.getName());
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo info) {
        super.onInitializeAccessibilityNodeInfo(info);
        info.setClassName(PinView.class.getName());
    }

    private void validatePin() {
        if (null != mPin && mPinCount < mPin.length()) {
            mPin = mPin.substring(0, mPinCount);
        }
    }

    public int getPinCount() {
        return mPinCount;
    }

    public void setPinCount(int mPinCount) {
        this.mPinCount = mPinCount;
        validatePin();
        invalidate();
    }

    public String getPin() {
        return mPin;
    }

    public void setPin(String mPinText) {
        this.mPin = mPinText;
        validatePin();
        invalidate();
    }

    public int getPinColor() {
        return mPinColor;
    }

    public void setPinColor(int mPinColor) {
        this.mPinColor = mPinColor;
        invalidate();
    }

    public boolean isHidePin() {
        return hidePin;
    }

    public void setHidePin(boolean hidePin) {
        this.hidePin = hidePin;
        invalidate();
    }

    public float getScalePinSize() {
        return scalePinSize;
    }

    public void setScalePinSize(float scalePinSize) {
        this.scalePinSize = scalePinSize;
        invalidate();
    }

    public float getScaleDotSize() {
        return scaleDotSize;
    }

    public void setScaleDotSize(float scaleDotSize) {
        this.scaleDotSize = scaleDotSize;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        initParams();
        drawPin(canvas);
    }

    private void initParams() {
        mWidth = getWidth();
        mHeight = getHeight();

        mPinSize = (float) mHeight * scalePinSize;
        mDotSize = (float) mHeight * scaleDotSize;
        mRawPinHeight = hidePin ? mDotSize : mPinSize;
        mDividerPinSize = mWidth / (mPinCount + 1) - mRawPinHeight;
        mPinPaint.setTextSize(mPinSize);
    }

    private void drawPin(Canvas canvas) {
        float dotsWidth = 0;
        for (int i = 0; i < mPinCount; i++) {
            if (i > 0) {
                dotsWidth += mDividerPinSize;
            }
            dotsWidth += mRawPinHeight;
        }
        float offset = (mWidth - dotsWidth) / 2;
        canvas.save();
        canvas.translate(offset, (mHeight - mRawPinHeight) / 2);
        float step = 0;
        for (int i = 0; i < mPinCount; i++) {
            if (i > 0) {
                Rect rect = new Rect((int) step, 0, (int) (step + mDividerPinSize), (int) mRawPinHeight);
                mPinPaint.setColor(Color.TRANSPARENT);
                canvas.drawCircle(rect.centerX(), rect.centerY(), mRawPinHeight / 2, mPinPaint);
                step += mDividerPinSize;
            }

            Rect rect2 = new Rect((int) step, 0, (int) (step + mRawPinHeight), (int) mRawPinHeight);
            mPinPaint.setColor(mPinColor);
            if (i >= getPinLength()) {
                mPinPaint.setStyle(Paint.Style.STROKE);
                mPinPaint.setStrokeWidth(getContext().getResources().getDisplayMetrics().density * 2);
                canvas.drawLine(rect2.left, rect2.bottom, rect2.right, rect2.bottom, mPinPaint);
            } else {
                mPinPaint.setStyle(Paint.Style.FILL);
                mPinPaint.setStrokeWidth(0);
                if (hidePin) {
                    canvas.drawCircle(rect2.centerX(), rect2.centerY(), mRawPinHeight / 2, mPinPaint);
                } else {
                    String key = String.valueOf(mPin.charAt(i));
                    Rect keyRect = new Rect();
                    mPinPaint.getTextBounds(key, 0, key.length(), keyRect);
                    float y = rect2.bottom -(rect2.height() - keyRect.height()) / 2;
                    if (isInEditMode()) {
                        y -= EDIT_MODE_OFFSET;
                    }
                    canvas.drawText(key, rect2.left, y, mPinPaint);
                }

            }

            step += mRawPinHeight;
        }
        canvas.restore();
    }

    private int getPinLength(){
        if (TextUtils.isEmpty(mPin)) {
            return 0;
        } else {
            return mPin.length();
        }
    }

    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        this.onKeyListener = onKeyListener;
    }

    @Override
    public boolean onKey(View v, int keyCodeInt, KeyEvent event) {
        boolean result = false;
        if (onKeyListener != null) {
            result = onKeyListener.onKey(v, keyCodeInt, event);
        }

        String keyCode = String.valueOf((char) event.getUnicodeChar());
        boolean isActionMultiple =  event.getAction() == KeyEvent.ACTION_MULTIPLE;
        boolean isActionUp =  event.getAction() == KeyEvent.ACTION_UP;

        if (!isActionMultiple && !isActionUp) {
            return result;
        } else if (event.isSystem() || isShiftUp(keyCodeInt) || isConfirmKeyUp(keyCodeInt)) {
            return result;
        }

        if (keyCodeInt == KeyEvent.KEYCODE_DEL) {
            deleteInternal();
        } else {
            if (isActionMultiple) {
                keyCode = event.getCharacters();
            }
            addInternal(keyCode);
        }
        return result;
    }

    private boolean isShiftUp(int keyCode) {
        return keyCode == KeyEvent.KEYCODE_SHIFT_LEFT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT;
    }

    public boolean isConfirmKeyUp(int keyCode) {
        return keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_NUMPAD_ENTER
                || keyCode == KeyEvent.KEYCODE_DPAD_CENTER;
    }

    private void addInternal(String keyCode) {
        mPin += keyCode;
        validatePin();
        invalidate();
    }

    private void deleteInternal() {
        if (mPin.length() > 0) {
            mPin = mPin.substring(0, mPin.length()-1);
            invalidate();
        }
    }

    private void showKeyboard(int flags) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(this, flags);
    }


    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        //Ignore
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        postDelayed(new Runnable() {
            @Override
            public void run() {
                showKeyboard(InputMethodManager.SHOW_IMPLICIT);
            }
        }, 200);
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        //Ignore
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }
}
